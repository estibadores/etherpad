FROM registry.sindominio.net/debian

ARG UID=107
ARG GID=65534

RUN apt update && \
	    apt-get install -y --no-install-recommends curl gnupg2 ca-certificates git

RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN NODE_MAJOR=20 && \
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update &&  apt-get install -y nodejs

RUN adduser \
   --system \
   --uid ${UID} \
   --gid ${GID} \
   --shell /usr/sbin/nologin \
   --gecos 'Etherpad' \
   --disabled-password \
   --home /etherpad \
   etherpad

WORKDIR etherpad

RUN git clone --branch master https://github.com/ether/etherpad-lite.git .

RUN . bin/functions.sh && \
	./bin/installDeps.sh && \
	chown -R etherpad:users /etherpad/var/

USER etherpad

ADD entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

EXPOSE 9001
CMD ["pnpm","run","prod"]
