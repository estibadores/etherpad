#!/bin/bash

## Exec : ETHERPAD_API_KEY='<api_key>' ./listPads.sh

[ -z $ETHERPAD_API_KEY ] && echo "Uso: ETHERPAD_API_KEY='<api_key>' ./listPads.sh"; exit 1

ETHERPAD_HOST='https://pad.sindominio.net'
ETHERPAD_API_VERSION='1.2.15' # can be found via https://pad.example.com/api
LIST_PADS_URL="${ETHERPAD_HOST}/api/${ETHERPAD_API_VERSION}/listAllPads?apikey=${ETHERPAD_API_KEY}"

PADS=`curl -s -X GET "${LIST_PADS_URL}" | jq -r '.data.padIDs[]'`

echo $PADS
