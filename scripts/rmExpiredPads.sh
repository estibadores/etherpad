#!/bin/bash

## Exec : ETHERPAD_API_KEY='<api_key>' ./rmExpiredPad.sh

if [ -z $ETHERPAD_API_KEY ]; then 
	echo "Uso: ETHERPAD_API_KEY='<api_key>' ./rmExpiredPads.sh"; 
	exit 1
fi

EXPIRED=60 # days
ETHERPAD_HOST='https://pad.sindominio.net'
ETHERPAD_API_VERSION='1.2.15' # can be found via https://pad.example.com/api
LIST_PADS_URL="${ETHERPAD_HOST}/api/${ETHERPAD_API_VERSION}/listAllPads?apikey=${ETHERPAD_API_KEY}"
DELETE_PAD_BASE="${ETHERPAD_HOST}/api/${ETHERPAD_API_VERSION}/deletePad?apikey=${ETHERPAD_API_KEY}"
LAST_EDITED_BASE="${ETHERPAD_HOST}/api/${ETHERPAD_API_VERSION}/getLastEdited?apikey=${ETHERPAD_API_KEY}"
PADS=`curl -s -X GET "${LIST_PADS_URL}" | jq -r '.data.padIDs[]'`
NOW=$(date +"%s")

echo "Borrado de los pads no editados los últimos ${EXPIRED} días"

for n in $PADS
do
	DT_LEDIT=`curl -s -X GET "${LAST_EDITED_BASE}&padID=$n" | jq -r '.data.lastEdited'`
	delta=$(($NOW - $DT_LEDIT/1000))
	days=$(($delta/60/60/24))
	echo "PadID:$n - $days dias"
	[ $days -gt $EXPIRED ] && curl -s -X GET "${DELETE_PAD_BASE}&padID=$n" || echo "El Pad no ha caducado"
	## TODO check if delete code is 0 ---- > jq -r '.code' -eq 0
done
