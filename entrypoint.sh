#!/bin/sh

set -ex

echo "Running directly, without checking/installing dependencies"

echo "Starting"

exec pnpm run prod "$@"
